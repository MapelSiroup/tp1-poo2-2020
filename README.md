Le nom des créateurs du projet : Florent Perreault et Olivier Girard

MIT License

Copyright (c) 2020 Florent Perreault

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


Ce projet est un logiciel de dessin qui, à l'aide de différents outils, permait de créer et dessiner des formes et figures différentes.

Tutoriel : Une fois le logiciel lancé, on peut voir qu'en haut du logiciel on a plusieurs onglet. Fichier permet d'obtenir plus de parametres tels que sauvegarder et charger un fichier.Il y a aussi Quitter qui est un sous-menu pour Fichier, qui permet de fermer le programme. L'onglet formes permet de choisir les différentes formes que l'on souhaite afficher sur le logiciel.
