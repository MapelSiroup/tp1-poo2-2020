package ca.cegepdrummond;

import java.awt.*;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.geom.GeneralPath;
import javax.swing.*;


import javax.swing.*;



public class Fenetre extends JPanel implements ActionListener {
    public static JFrame jf;
    public Fenetre()  {
        jf =  new JFrame("Dessin");    //Titre du logiciel
        Container cp = jf.getContentPane();
        Canvas canvas = new Canvas();
        cp.add(canvas);
        jf.setSize(970, 720); // Ajuste les dimensions de l'application
        jf.setUndecorated(true);
        jf.setResizable(false);
        Menu();
        jf.setVisible(true); // Rend l'application visible
        jf.setLocationRelativeTo(null);  //Centre la fenetre au milieu de l'ecran ne pas enlever le commentaire sinon ya pas le menubar
    }

    public static void Menu() {
        JMenuBar menuBar;
        JMenu Fichier, Formes, Sauvegarder, Charger; // Menus et sous menus
        JMenuItem FaireTri, FaireCercle, FaireCarree, FaireRectangle, FaireLigne, FaireElipse, Quitter; // Items du Menu
        menuBar = new JMenuBar();

        Fichier = new JMenu("Fichier"); //Instancie le menu
        Fichier.setMnemonic(KeyEvent.VK_F);     //souligne le F de "Fichier"
        menuBar.add(Fichier);   //ajoute le menu "Fichier"

        Formes = new JMenu("Formes"); //Instancie le menu
        Formes.setMnemonic(KeyEvent.VK_O);     //souligne le F de "Formes"
        menuBar.add(Formes);   //ajoute le menu "Formes"
        //Cercle
        FaireCercle = new JMenuItem("Cercle"); //Instancie le menu
        FaireCercle.setMnemonic(KeyEvent.VK_C);     //souligne le F de "Formes"
        Formes.add(FaireCercle);   //ajoute le menu "Formes"
        FaireCercle.addActionListener((event) -> new dessinerCercle());//paintComponent());
        //Triangle
        FaireTri = new JMenuItem("Triangle"); //Instancie le menu
        FaireTri.setMnemonic(KeyEvent.VK_T);     //souligne le F de "Formes"
        Formes.add(FaireTri);   //ajoute le menu "Formes"
        FaireTri.addActionListener((event) -> new dessinerCercle());//paintComponent());
        //Rectangle
        FaireRectangle = new JMenuItem("Rectangle"); //Instancie le menu
        FaireRectangle.setMnemonic(KeyEvent.VK_R);     //souligne le F de "Formes"
        Formes.add(FaireRectangle);   //ajoute le menu "Formes"
        FaireRectangle.addActionListener((event) -> new dessinerCercle());//paintComponent());
        //Carree
        FaireCarree = new JMenuItem("Carré"); //Instancie le menu
        FaireCarree.setMnemonic(KeyEvent.VK_A);     //souligne le F de "Formes"
        Formes.add(FaireCarree);   //ajoute le menu "Formes"
        FaireCarree.addActionListener((event) -> new dessinerCarre());//paintComponent());
        // Carree
        FaireLigne = new JMenuItem("Ligne"); //Instancie le menu
        FaireLigne.setMnemonic(KeyEvent.VK_L);     //souligne le L de "Lignes"
        Formes.add(FaireLigne);   //ajoute le menu "Formes"
        FaireLigne.addActionListener((event) -> new dessinerLigne());//paintComponent());

        Sauvegarder = new JMenu("Sauvegarder"); // Instancie le menu
        Fichier.add(Sauvegarder); // Ajoute Sauvegarder en sous-menu de fichier

        Charger = new JMenu("Charger"); // Instancie le menu
        Fichier.add(Charger); // Ajoute Charger en sous-menu de fichier
        Fichier.addSeparator(); //Separe les deux premiers submenu

        Quitter = new JMenuItem("Quitter"); //instancie le menu
        Quitter.setMnemonic(KeyEvent.VK_Q);     //souligne le Q de "Quitter"
        Fichier.add(Quitter);   //Ajoute le menu Fichier
        Quitter.addActionListener((event) -> System.exit(0)); // Permet de quitter l'application avec Quitter System.exit(0)

        jf.setJMenuBar(menuBar); // Initialise menuBar
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}