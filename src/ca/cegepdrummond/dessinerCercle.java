package ca.cegepdrummond;

import java.awt.Graphics;
import javax.swing.*;

/**
 * Permet de dessiner un cercle
 */
public class dessinerCercle extends JPanel {
    public void paint(Graphics g) {

        g.drawOval(0, 0, 300, 300);
    }
}
