package ca.cegepdrummond;

import java.awt.*;
import java.awt.event.KeyEvent;
import javax.swing.*;


public class Main extends Canvas {
    //private static JFrame frame;
    public static void main(String[] args) {
        Fenetre window = new Fenetre();

        JFrame g = new JFrame("Dessiner un rectangle");
        g.getContentPane().add(new dessinerRectangle());
        g.setSize(425, 425);
        g.setVisible(true);
        g.setLocationRelativeTo(null);

        JFrame fr = new JFrame("Dessiner un cercle");
        fr.getContentPane().add(new dessinerCercle());
        fr.setSize(340, 340);
        fr.setVisible(true);
        fr.setResizable(false);
        fr.setLocationRelativeTo(null);

        JFrame frt = new JFrame("Dessiner un Triangle");
        frt.getContentPane().add(new dessinerTriangle());
        frt.setSize(300, 300);
        frt.setVisible(true);
        frt.setResizable(false);
        frt.setLocationRelativeTo(null);

        JFrame f = new JFrame("Dessiner une ligne");
        f.getContentPane().add(new dessinerLigne());
        f.setSize(250, 250);
        f.setVisible(true);
        f.setResizable(false);
        f.setLocationRelativeTo(null);
    }
}
